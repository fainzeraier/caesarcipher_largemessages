///-----------------------------------------------------------------------------
///
///\copyright Copyright(C) Henrik Hyvärinen I guess
///
///\file    CaesarCipher.h
///\brief   Header for Caesar Cipher containing Encrypt and Decrypt functions
///
///\author  Henrik Hyvärinen
///\date    8.2.2019
///
///\details Contains functions for encrypting and decrypting messages using the
///         Caesar Cipher (https://en.wikipedia.org/wiki/Caesar_cipher). It
///         shifts all characters right when encrypting, and left when
///         decrypting. The implementation alters the message directly and does
///         not return any values.
///-----------------------------------------------------------------------------

#ifndef CaesarCipher_h
#define CaesarCipher_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

///\brief Perform encrypt/decrypt based on string parameters, e.g. command line parameters.
///
///\param argumentCount Number of arguments
///\param argumentValues Arguments as char arrays (aka strings). First argument is ignored,
///       the following values are: 1. encryption key, 2. either "crypt" or "decrypt", 3. the message which should
///       contain only a-z letters
///
///\returns a char array allocated using malloc. Be sure to free it afterwards
char* Caesar_UsingStringParameters(int argumentCount, char *argumentValues[]);

///\brief Decrypt a message by shifting every character in the message left by encryptionKey places.
///
///\param encryptionKey Key indicating number of places to shift each character left.
///\param message The message to decrypt. Assumed to contain only lowercase characters from a to z
///\param messageLength Number of characters in message.
///\param char array where the encrypted message is stored. This must be modifiable, and can be same as message parameter.
void Caesar_Encrypt(int encryptionKey, char *message, int messageLength, char *modified_message);

///\brief Encrypt a message by shifting every character in the message right by encryptionKey places.
///
///\param encryptionKey Key indicating number of places to shift each character right.
///\param message The message to decrypt. Assumed to contain only lowercase characters from a to z
///\param messageLength Number of characters in message.
///\param char array where the decrypted message is stored. This must be modifiable, and can be same as message parameter.
void Caesar_Decrypt(int encryptionKey, char *message, int messageLength, char *modified_message);

#endif
