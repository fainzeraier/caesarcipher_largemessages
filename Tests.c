#include "CaesarCipher.h"
#include "minunit.h"
#include <stdio.h>
#include <time.h>
#include <string.h>

char largeMessage[100000000];

void PerformanceTest()
{
   double total_time;
   clock_t start, end;
   int i;
   int characterCount = 100000000;

   for (i = 0; i < 100000000; i++)
   {
      largeMessage[i] = 'a' + i % 26;
   }

   start = clock(); //time count starts
   Caesar_Encrypt(12, largeMessage, characterCount, largeMessage);
   end = clock();                                  //time count stops
   total_time = ((double)(end - start)) / CLK_TCK; //calulate total time

   printf("\nPERFORMANCE TEST\nEncrypt run time: %f seconds\nCharacters: %i\n", total_time, characterCount);
   printf("Characters per ms: %.1f", characterCount / 1000 / total_time);

   start = clock();
   Caesar_Decrypt(12, largeMessage, characterCount, largeMessage);
   end = clock();
   total_time = ((double)(end - start)) / CLK_TCK; //calulate total time

   printf("\n\nDecrypt run time: %f seconds\nCharacters: %i\n", total_time, characterCount);
   printf("Characters per ms: %.1f", characterCount / 1000 / total_time);
}

static char *TestEncrypt()
{
   char message[] = "attackatonce";
   char message_compare[] = "exxegoexsrgi";
   Caesar_Encrypt(4, message, strlen(message), message);
   mu_assert("error, Encrypt 4 'attackatonce' did not return 'exxegoexsrgi'", strcmp(message, message_compare) == 0);
   return 0;
}

static char *TestDecrypt()
{
   char message[] = "exxegoexsrgi";
   char message_compare[] = "attackatonce";
   Caesar_Decrypt(4, message, strlen(message), message);
   mu_assert("error, Decrypt 4 'exxegoexsrgi' did not return 'attackatonce'", strcmp(message, message_compare) == 0);
   return 0;
}

static char *TestDecryptWrap()
{
   char message[] = "abcde";
   char message_compare[] = "vwxyz";
   Caesar_Decrypt(5, message, strlen(message), message);
   mu_assert("error, Decrypt 5 'abcde' did not return 'vwxyz'", strcmp(message, message_compare) == 0);
   return 0;
}

static char *TestEncryptWrap()
{
   char message[] = "vwxyz";
   char message_compare[] = "abcde";
   Caesar_Encrypt(5, message, strlen(message), message);
   mu_assert("error, Encrypt 5 'vwxyz' did not return 'abcde'", strcmp(message, message_compare) == 0);
   return 0;
}

static char *TestEncryptNegative()
{
   char message[] = "attackatonce";
   char message_compare[] = "exxegoexsrgi";
   Caesar_Encrypt(-22, message, strlen(message), message);
   mu_assert("error, Encrypt -22 'attackatonce' did not return 'exxegoexsrgi'", strcmp(message, message_compare) == 0);
   return 0;
}

static char *TestEncryptLarge()
{
   char message[] = "attackatonce";
   char message_compare[] = "exxegoexsrgi";
   Caesar_Encrypt(2604, message, strlen(message), message);
   mu_assert("error, Encrypt 2604 'attackatonce' did not return 'exxegoexsrgi'", strcmp(message, message_compare) == 0);
   return 0;
}

char *input_crypt[] = {"./a.exe", "4", "crypt", "attackatonce"};

static char *TestInput()
{
   char *result = Caesar_UsingStringParameters(4, input_crypt);
   mu_assert("error, input_crypt '4 crypt attackatonce' did not yield 'exxegoexsrgi'", strcmp(result, "exxegoexsrgi") == 0);
   free(result);
   return 0;
}

char *input_wrong[] = {"./a.exe"};

static char *TestInputEmpty()
{
   char *result = Caesar_UsingStringParameters(1, input_wrong);
   mu_assert("error, empty input did not return NULL pointer", result == NULL);
   free(result);
   return 0;
}

static char *AllTests()
{
   mu_run_test(TestEncrypt);
   mu_run_test(TestDecrypt);

   mu_run_test(TestEncryptWrap);
   mu_run_test(TestDecryptWrap);

   mu_run_test(TestEncryptLarge);
   mu_run_test(TestEncryptNegative);

   mu_run_test(TestInput);
   mu_run_test(TestInputEmpty);
   return 0;
}

int main(int argc, char *argv[])
{
   char *result = AllTests();
   if (result != 0)
   {
      printf("%s\n", result);
   }
   else
   {
      printf("\nALL TESTS PASSED");
   }
   printf("\nTests run: %d\n\n", tests_run);

   // Run test for performance with message of 100,000,000 a-z letters
   PerformanceTest();

   printf("\n\nTest with command line input:");
   char *input_result = Caesar_UsingStringParameters(argc, argv);
   printf(input_result);

   getchar();

   return result != 0;
}
