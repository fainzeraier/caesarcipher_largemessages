#include "CaesarCipher.h"

char *Caesar_UsingStringParameters(int argument_count, char *argument_values[])
{
   if (argument_count != 4)
   {
      printf("Error: expected 4 arguments, got %i", argument_count);
      return NULL;
   }

   const char *message_pointer = argument_values[1];
   char *end_pointer = NULL;
   long encryption_key = 0;

   errno = 0; // reset errno to 0 before call

   // strtol attempts to convert message to long, and sets end_pointer and errno to use for error checking
   encryption_key = strtol(message_pointer, &end_pointer, 10);

   // Check problems. 1. if message_pointer and end_pointer are same, no characters were read
   if (message_pointer == end_pointer)
   {
      printf("Encryption key invalid  (no digits found, 0 returned)\n");
      return NULL;
   }
   // 2. Check for overflow and underflow
   else if (errno == ERANGE && (encryption_key == LONG_MIN || encryption_key == LONG_MAX))
   {
      printf("Encryption key invalid  (underflow or overflow occurred)\n");
      return NULL;
   }
   // 3. If some other error happened, check if received key is 0
   else if (errno != 0 && encryption_key == 0)
   {
      printf("Encryption key invalid (unspecified error occurred)\n");
      return NULL;
   }

   bool encrypt;

   // strcmp returns 0 only when the strings match
   if (strcmp(argument_values[2], "crypt") == 0)
   {
      encrypt = true;
   }
   else if (strcmp(argument_values[2], "decrypt") == 0)
   {
      encrypt = false;
   }
   else
   {
      printf("Error: did not receive either 'crypt' or 'decrypt' instruction");
      return NULL;
   }

   char *message = argument_values[3];
   int message_length = strlen(message);

   // Allocate memory for the return message. 
   // It doesn't need to be initialized except for last char which must be '\0' for string operations to work.
   char *message_copy = (char *)malloc((message_length + 1) * sizeof(char));
   if (message_copy == NULL)
   {
      printf("Error: Failed to allocate memory for return message");
      return NULL;
   }
   message_copy[message_length] = '\0';

   if (encrypt)
   {
      Caesar_Encrypt(encryption_key, message, message_length, message_copy);
   }
   else
   {
      Caesar_Decrypt(encryption_key, message, message_length, message_copy);
   }

   return message_copy;
}

void Caesar_Encrypt(int encryption_key, char *message, int message_length, char *modified_message)
{
   // The operation is more efficient when done as a left shift instead of right shift,
   // because when right shifting the char may overflow (z=122, char max value 127)
   int leftShift;

   if (encryption_key > 0)
   {
      // For positive keys, use modulo to place the key in the 0..25 range. Then subtract key from 26 to change it to left shift
      leftShift = 26 - encryption_key % 26;
   }
   else
   {
      // For negative: Multiply by -1 to make key positive, then take modulo 26 to bring it in the 0..25 range.
      leftShift = (-1 * encryption_key) % 26;
   }

   for (int i = 0; i < message_length; i++)
   {
      // Do left shift, then if below 'a', increase by 26 to bring it back within a-z range.
      modified_message[i] = message[i] - leftShift;
      if (modified_message[i] < 'a')
      {
         modified_message[i] += 26;
      }
   }
}

void Caesar_Decrypt(int encryption_key, char *message, int message_length, char *modified_message)
{
   // Decrypt is the same as encrypt, except it's a left shift instead of right shift.
   int invertedKey = -1 * encryption_key;
   Caesar_Encrypt(invertedKey, message, message_length, modified_message);
}
