#include <stdio.h>
#include "CaesarCipher.h"
#include <stdlib.h>

int main(int argc, char *argv[])
{
   char *result = Caesar_UsingStringParameters(argc, argv);
   if(result != NULL){
      printf(result);
   }
   free(result);
}